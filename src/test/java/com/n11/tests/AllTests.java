package com.n11.tests;

import com.n11.base.BaseTest;
import com.n11.pages.FavouritesPage;
import com.n11.pages.HomePage;
import com.n11.pages.LoginPage;
import com.n11.pages.SearchPage;
import org.testng.annotations.Test;

public class AllTests extends BaseTest {

    @Test
    public void allTestCases() {

        // Opening Homepage
        HomePage homePage = new HomePage(driver);
        homePage.open();
        sleep(3000);

        // Verifying Homepage
        homePage.verifyHomePage();

        // Opening Login Page
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        sleep(3000);

        // Enter username and password
        loginPage.loginUser("n11testcase@gmail.com", "Testn11case");

        // Click on login button
        loginPage.clickOnButton();
        sleep(3000);

        // Search product
        SearchPage searchPage = new SearchPage(driver);
        searchPage.searchProduct("samsung");
        sleep(3000);

        // Click on search button
        searchPage.clickOnSearchButton();
        sleep(3000);

        // Verify if product is found
        // searchPage.productVerification(); //alternative
        searchPage.productVerification2();

        // Open Second Page
        searchPage.clickOnSecondPage();
        sleep(3000);

        // Verify if second page is opened
        searchPage.verifySecondPageIsOpened();
        sleep(3000);

        // Add third product from top to the favourites list
        searchPage.addThirdProductFromTopInList();
        sleep(3000);

        // Click on favourites on top of page
        FavouritesPage favouritesPage = new FavouritesPage(driver);
        favouritesPage.clickOnFavouritesList();
        sleep(3000);

        // Verify If product is added to the list
        favouritesPage.productVerificationInList();
        sleep(3000);

        // Remove product from my favourites list
        favouritesPage.removeProductFromMyFavourites();
        sleep(3000);

        // Verify if product is removed successfully from the list
        favouritesPage.removalMessage();
        sleep(3000);
    }
}
