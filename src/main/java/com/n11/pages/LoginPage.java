package com.n11.pages;

import com.n11.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePageObject {

    By usernameLocator = By.xpath("/html//input[@id='email']");
    By passwordLocator = By.xpath("/html//input[@id='password']");
    By logInButtonLocator = By.xpath("/html//div[@id='loginButton']");
    By openLoginPage = By.cssSelector("a[title='Giriş Yap']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    // Open login Page
    public void open() {
        find(openLoginPage).click();
    }

    // Enter user credentials
   public void loginUser (String username, String password) {
        find(usernameLocator).sendKeys(username);
        find(passwordLocator).sendKeys(password);
   }

   // Click on login button
   public void clickOnButton() {
        find(logInButtonLocator).click();
   }
}
