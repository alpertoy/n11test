package com.n11.pages;

import com.n11.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class FavouritesPage extends BasePageObject {

    By myAccountLocator = By.xpath("/html//header[@id='header']/div[@class='container']//div[@class='customMenu']/div[2]/div[@class='myAccount']/a[1]");
    By myFavouritesAndListsLocator = By.xpath("/html//div[@id='myAccount']//ul//a[@href='https://www.n11.com/hesabim/istek-listelerim']");
    By verifyProductIsInListLocator = By.cssSelector(".listItemProductList img");
    By myFavouritesLocator = By.xpath("//div[@id='myAccount']//ul[@class='wishGroupList']//a[@href='https://www.n11.com/hesabim/favorilerim']/h4[@class='listItemTitle']");
    By removeProductButtonLocator = By.xpath("//div[@id='view']/ul/li/div//span[@class='deleteProFromFavorites']");
    By successfullyRemovedMessage = By.xpath("//body/div[@class='lightBox']//span[@class='message']");

    public FavouritesPage(WebDriver driver) {
        super(driver);
    }

    // Click on favourites list
    public void clickOnFavouritesList() {
        find(myAccountLocator).click();
        find(myFavouritesAndListsLocator).click();
    }

    // Verify if product is in list
    public Boolean productVerificationInList() {
        try {
            return driver.findElement(verifyProductIsInListLocator).isDisplayed();
        } catch (org.openqa.selenium.NoSuchElementException exception) {
            return false;
        }
    }

    // Remove product from my favourites
    public void removeProductFromMyFavourites() {
        find(myFavouritesLocator).click();
        find(removeProductButtonLocator).click();
    }

    // Verify if product is removed from the list
    public void removalMessage() {
        find(successfullyRemovedMessage).isDisplayed();
    }

}
