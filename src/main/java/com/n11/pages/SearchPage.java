package com.n11.pages;

import com.n11.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class SearchPage extends BasePageObject {

    By searchBoxLocator = By.xpath("/html//input[@id='searchData']");
    By searchButtonLocator = By.cssSelector("a[title='ARA'] > .icon.iconSearch");
    By productFoundLocator = By.cssSelector(".resultText");
    By productFoundLocator2 = By.cssSelector(".productArea");
    By secondPageLocator = By.cssSelector(".pagination > a:nth-of-type(2)");
    By thirdProductFromTop = By.xpath("//div[@id='view']/ul/li[3]/div//span[@title='Favorilere ekle']");

    private String secondPageUrl = "https://www.n11.com/arama?q=samsung&pg=2";

    public SearchPage(WebDriver driver) {
        super(driver);
    }

    // Search product
    public void searchProduct(String productName) {
        find(searchBoxLocator).sendKeys(productName);
    }

    // Click on search product button
    public void clickOnSearchButton() {
        find(searchButtonLocator).click();
    }

    // Verify if product is found
    public Boolean productVerification() {
        try {
            return driver.findElement(productFoundLocator).isDisplayed();
        } catch (org.openqa.selenium.NoSuchElementException exception) {
            return false;
        }
    }

    // Verify if product is found (alternative)
    public Boolean productVerification2() {
        try {
            return driver.findElement(productFoundLocator2).isDisplayed();
        } catch (org.openqa.selenium.NoSuchElementException exception) {
            return false;
        }
    }

    // Click on second page
    public void clickOnSecondPage() {
        find(secondPageLocator).click();
    }

    // Verify if second page is opened
    public void verifySecondPageIsOpened() {
        String actualUrl = driver.getCurrentUrl();
        Assert.assertEquals(actualUrl, secondPageUrl, "Actual page url is not the same as expected");
    }

    public void addThirdProductFromTopInList() {
        find(thirdProductFromTop).click();
    }

}
