package com.n11.pages;

import com.n11.base.BasePageObject;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class HomePage extends BasePageObject {

    private String homePageUrl = "https://www.n11.com/";

    public HomePage(WebDriver driver) {
        super(driver);
    }

    // Open homepage by url
    public void open() {
        openUrl(homePageUrl);
    }

    // Verify homepage is opened
    public void verifyHomePage() {
        String actualUrl = driver.getCurrentUrl();
        Assert.assertEquals(actualUrl, homePageUrl, "Actual page url is not the same as expected");
    }
}
