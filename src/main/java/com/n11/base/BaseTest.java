package com.n11.base;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

public class BaseTest {

    protected WebDriver driver;

    @BeforeTest(alwaysRun = true)
    @Parameters({ "browser" })
    protected  void setUp(@Optional("firefox") String browser) {
        BrowserDriverFactory factory = new BrowserDriverFactory(browser);
        driver = factory.createDriver();

        sleep(3000);

        // Maximize browser window
        driver.manage().window().maximize();

    }

    @AfterMethod(alwaysRun = true)
    protected void terminate() {
        // Closing driver
        driver.quit();
    }

    protected void sleep(long m) {
        try {
            Thread.sleep(m);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
